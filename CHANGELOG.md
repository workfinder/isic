# Changelog

## 1.0.4

* Simplify the build process


## 1.0.0

* Initial release.
